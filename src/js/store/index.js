//a redux store wrap up the state of the app
//State in redux comes from reducers (reducers produce the state of application.)

import { createStore, applyMiddleware, compose } from "redux";
import rootReducer from "../reducers/index";
import thunk from 'redux-thunk';


const storeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

 /* eslint-disable no-underscore-dangle */
const store = createStore(rootReducer,
    storeEnhancers(applyMiddleware(thunk)));
/* eslint-enable */
export default store;