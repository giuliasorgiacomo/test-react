import React from 'react';
import { setMaxRate } from "../actions/index";
import { connect } from "react-redux";

function mapDispatchToProps(dispatch) {
    return {
      changeRate: maxrate => dispatch(setMaxRate(maxrate))
    };
}

class Plafond extends React.Component {

    handleChange(event) {  
        this.props.handleChangeValue(event.target.value); 
        this.props.changeRate(event.target.value);
    }

    render() {
        return (
            <div className="containerPlatfond center">
                <div className="block">
                    <div className="imput-plafond">
                        <p>Inserisci il Plafond della tua carta Nexi:</p>
                        <div className="box-plafond">
                            <input type="text" minrate={this.props.minrate} className="plafond" value={this.props.inputValue} onChange={event => this.handleChange(event)} /> €
                        </div>
                    </div>
                </div>
                
            <div className={this.props.errorClass?"error hidden":"error show"}>Attenzione: il tuo plafond deve essere superiore all'importo minimo rateizzabile di {this.props.minrate}€</div>
            </div>
        )
    }
}
const PlafondRes = connect(null, mapDispatchToProps)(Plafond);
export default PlafondRes;
