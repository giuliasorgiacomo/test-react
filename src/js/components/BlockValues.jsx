import React from 'react';
import { setFinalResult } from "../actions/index";

class BlockValues extends React.Component {

    render() {
        return (
            <div className="block-calculator block-values">
                  <strong>Rata mensile: <span className="rata-mensile">{this.props.monthRate}</span>€</strong>
                  <p>inclusa commissione mensile di <span className="commissione-mensile">{this.props.monthCommission}</span>€</p>
                  <p className="total"><em>Commissione totale per il piano selezionato (importo/durata): <span className="commissione-totale">{this.props.totalCommission}</span><span>€</span></em></p>
            </div>
        );
    }
}

export default BlockValues;