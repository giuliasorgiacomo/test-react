import React from "react";
import Header from "./Header.jsx";
import Title from "./Title.jsx";
import ConfiguratorContainer from "../containers/ConfiguratorContainer.jsx";

const App = () => (
    
    <div>
        <Header />
        <div className="simulator">
            <Title />
            <ConfiguratorContainer />
        </div>
    </div> 
);
export default App;