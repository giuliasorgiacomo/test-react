import React, { Component } from 'react';
import hero from '../../images/hero-desk.jpg'

class Header extends Component {
    render() {
        let imgUrl = {hero};
        let styles = {
            root: {
                background: 'url(' + imgUrl + ')',
                backgroundSize: 'cover',
                overflow: 'hidden',
            }};
        return (
            <div style= {styles.root} className="hero">
                <div className="hero-content">
                    <p>
                    Un giorno<br/>
                    potrai decidere<br/>
                    cosa comprare<br/>
                    e quando pagare.<br/>
                    </p>
                </div>
            </div>
        )
    }
}

export default Header;