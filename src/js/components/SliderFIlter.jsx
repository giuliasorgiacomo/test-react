import React from 'react';
import Slider from 'rc-slider';
import 'rc-slider/assets/index.css';
import { getResult } from "../actions/index";
import { connect } from "react-redux";

function mapDispatchToProps(dispatch) {
    return {
        calcRate: (type) => dispatch(getResult(type))
    };
}

class SliderFIlter extends React.Component {

    onAfterChangeHandler = (value) => {
        var type = this.props.isTime ? "time" : "rate";
        this.props.calcRate({ value, type });
    };

    render() {

        return (
            <div className="input-range-container">
                <Slider isTime={this.props.isTime} dots={this.props.dots} isRate={this.props.isRate} min={this.props.min} step={this.props.step} marks={this.props.marks} max={this.props.max} onAfterChange={this.onAfterChangeHandler}  />
            </div>
                    
        );
    }
}
const slider = connect(null, mapDispatchToProps)(SliderFIlter);
export default slider;