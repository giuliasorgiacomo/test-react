import React, { Component } from 'react';
import SliderFIlter from "./SliderFIlter.jsx";

export class Calculator extends Component {

    render() {
        return (
            <div className="block-calculator">
                <div className={this.props.errorClass ?"block-error show":"block-error"}></div>
                <div className="secondo"> SCEGLI L'IMPORTO DELL'ACQUISTO DA RATEIZZARE </div>
                <div className="container import-setting"><SliderFIlter isTime={false} isRate={true} dots={false} min={this.props.min} step={50} marks={this.props.rate} max={this.props.max} defaultRateValue={this.props.defaultRateValue} /></div>
                <div className="secondo"> IN QUANTO TEMPO VUOI PAGARE?</div>
                <div className="container import-setting"><SliderFIlter  isTime={true} isRate={false} dots={false} min={3} marks={this.props.time} step={null} max={this.props.maxtime}/></div>
            </div>
        )
        
    }
    
}

export default Calculator;