/*
Since strings are prone to typos and duplicates it’s better to have action types declared as constants.
*/

export const CHANGE_RATE = "CHANGE_RATE";