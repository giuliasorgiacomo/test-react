import React from "react";
import Calculator from "../components/Calculator.jsx";
import Platfond from "../components/Platfond.jsx";
import BlockValues from "../components/BlockValues.jsx";
import { fetchDefaultvalue,setFinalResult } from "../actions/index";
import { connect } from 'react-redux'


class ConfiguratorContainer extends React.Component {

  constructor (props) {
    super(props);
    this.state = {
      showerror: true
    } 
  }

  handleChange(event,valuemin) {  
    if( valuemin <= event) {
        this.setState({showerror:false})
    } else{
      this.setState({showerror:true})
    }
  }
  handleChangeSlider(event,rate,time) {  
    console.log(rate)
  }
  componentDidMount() {
    this.getData();
  }
  getData(){
    this.props.fetchDefaultvalue();
  }
  
  render() {
    return (
      <React.Fragment>
        <div className="block-plafond">
            <Platfond errorClass={!this.state.showerror} minrate={this.props.min} handleChangeValue={event => this.handleChange(event,this.props.min)} />
        </div>
        <Calculator handleChangeSlider={event => this.handleChangeSlider(event,this.props.actRate,this.props.actTime)} errorClass={this.state.showerror} min={this.props.min} maxtime={this.props.timeCounter} time={this.props.time} rate={this.props.rate} max={this.props.max} defaultRateValue={this.props.default}/>
        <BlockValues monthRate={this.props.monthRate} monthCommission={this.props.monthCommission} totalCommission={this.props.totalCommission}/>
      </React.Fragment>
    )
  }
}

function mapStateToProps(state) {
  return {
    rate: state.rate,
    max: state.max,
    min: state.min,
    time: state.time,
    timeCounter: state.timeCounter,
    default: state.default,
    actRate: state.actRate,
    actTime: state.actTime,
    monthRate: state.monthRate,
    monthCommission: state.monthCommission,
    totalCommission: state.totalCommission
  };
}

export default connect(
  mapStateToProps,
  { fetchDefaultvalue }
)(ConfiguratorContainer);
