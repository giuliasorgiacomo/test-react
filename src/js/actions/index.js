import store from '../store/index.js';

/*

The second principle of Redux says the only way to change the state is by sending a signal to 
the store.This signal is an action. “Dispatching an action” is the process of sending out a 
signal. Redux actions are nothing more than JavaScript objects

    STATE DESCRIPTION:
    max: max rate value
    min: min rate value
    rate: rate range
    time: time range
    timeCounter: //time cursor position
    default: //rate cursor position
    actRate: //actual selected rate
    actTime: //actual selected time
    monthRate: // month rate result of calc rate
    monthCommission: //commission result of calc rate
    totalCommission: //total commissionresult of calc rate

*/

function fetchPostsRequest(){
  return {
    type: "FETCH_REQUEST"
  }
}

//after json call
function fetchPostsSuccess(payload,param) {
  
  var paramDefault = payload.default;
  var res =payload.rate;
  var lastParam;
  var defaultValue = param;
  var time;
  var min;
  var max;
  var timeCounter = 1;
  var BreakException = {};

  if(isNaN(param)) defaultValue = 2000;

  //get default max and min values
  Object.keys(paramDefault).forEach(function (item) {
    switch (paramDefault[item].pos) {
        case 'min':
        min=paramDefault[item].value;
        break
        default:
        max=paramDefault[item].value;
    }
  })
    
  try {
    //Get time and rate ranges
    Object.keys(res).forEach(function (item) {
      time = res[item];
      if(!isNaN(param)) {
        paramDefault[item] = {label : item + " €", value : parseInt(item), pos: "max"};
        lastParam =  parseInt(item);
      }
      if(item>param) {
        throw BreakException;
      }
      timeCounter ++
    });
    
  }catch (e) {
    if (e !== BreakException) throw e;
  }

  Object.keys(time).forEach(function(key, i) {
    timeCounter = key;
  });

  if(isNaN(param) || param > max) {
    lastParam = max
  }

  //calc rate function
  var result = setFinalResult (3,defaultValue,defaultValue);

  return {
    type: "FETCH_SUCCESS",
    max:lastParam,
    min:min,
    rate:paramDefault,
    time:time,
    timeCounter: parseInt(timeCounter),
    default:defaultValue,
    actRate: defaultValue,
    actTime: 3,
    monthRate: result.rataMensile,
    monthCommission: result.commissione,
    totalCommission: result.commissioneTotale
  }
}

function fetchPostsError() {
  return {
    type: "FETCH_ERROR"
  }
}
 
export function fetchPostsWithRedux(param) {
  return (dispatch) => {
    dispatch(fetchPostsRequest());
    return fetchPosts().then(([response, json]) =>{
        if(response.status === 200){
        dispatch(fetchPostsSuccess(json,param.value))
      }
      else{
        dispatch(fetchPostsError())
      }
    })
  }
}

function fetchPosts() {
  const URL = "/rates.json";
  return fetch(URL, { method: 'GET'})
  .then( response => Promise.all([response, response.json()]));
}

//used in platfond
export function setMaxRate(param) {
  return fetchPostsWithRedux({"value":parseInt(param)})
}

//used in configuratorContainer at first init
export function fetchDefaultvalue() {
  return fetchPostsWithRedux({"value":"default"})
}

//used in sliderFilter to change rate and time ranges
export function getResult(type){
  return (dispatch) => {
    dispatch(changeStatusSlider(type))
  }
}


export function changeStatusSlider(param) {
  return (dispatch) => {
    dispatch(setBlockValues())
  
    if(param.type==="time") {
      return dispatch(changeTime(param.value))
    }else {
      return dispatch(changeRate(param.value))
    }
  }
}

function changeTime(param) {
  return {
    type: "CHANGE_TIME",
    actTime:param.value
  }
}

function changeRate(param) {
  return {
    type: "CHANGE_RATE",
    actTime:param.value
  }
}

function setBlockValues (){
  var newState =store.getState();
  var result = setFinalResult (newState.actTime,newState.actRate,newState.default)
  return {
    type: "SET_COMMISSION",
    monthRate: result.rataMensile,
    monthCommission: result.commissione,
    totalCommission: result.commissioneTotale
  }
}

//cal final rates
function setFinalResult (mesi,importo,plafond){
  var commissione = 0;
  var schemaV1 = [1];
  var schemaV2 = [2,2.5];
  var schemaV3 = [3,4,4];
  var schemaV4 = [4,5,5,5];
  var schemaV5 = [5,6,7,7,7];
  var schemaV6 = [6,8,9,9,9,9];
  var schemaV7 = [8,11,11,11,11,11];

  var schema = [];
  var max = 6;
  schema = schemaV7;

  if(importo <= 500){
      max = 1;
      schema = schemaV1;
  }else if(importo <= 750){
      max = 2;
      schema = schemaV2;
  }else if(importo <= 1000){
      max = 3;
      schema = schemaV3;

  }else if(importo <= 1250){
      max = 4;
      schema = schemaV4;
  }else if(importo <= 1500){
      max = 5;
      schema = schemaV5;
  }else if(importo <= 2000){
      max = 6;
      schema = schemaV6;
  }

  if(mesi > max){
      mesi = max;
  }

  commissione = schema[mesi - 1];
  var numeroMesi = [3,6,9,12,18,24];
  var nMesi = numeroMesi[mesi - 1];
  var rataMensile = importo / nMesi + commissione;
  var commissioneTotale =  nMesi * commissione;
  var plafondSecondoMese = plafond - importo + rataMensile - commissione;

  console.log('numeroMesi: ' + nMesi);
  console.log('rataMensile: ' + rataMensile);
  console.log('commissioneMensile: '+ commissione);
  console.log('commissioneTotale: ' + commissioneTotale);
  console.log('plafondSecondoMese: ' + plafondSecondoMese);
  console.log('plafond: ' + plafond);
  console.log('importo: ' + importo);
  console.log('commissione: ' + commissione);
   
  return {commissione: commissione,rataMensile:rataMensile, commissioneTotale:commissioneTotale}

}

store.subscribe(setBlockValues)