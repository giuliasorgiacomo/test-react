/*
A reducer is just a JavaScript function. iT takes two parameters: 
the current state and an action.
The third principle of Redux says that the state is immutable and cannot change in place.
This is why the reducer must be pure. A pure function is one that returns the exact same 
output for the given input.
*/

function rootReducer(state = {}, action) {
    switch (action.type) {
        case "FETCH_REQUEST":
            return state;
        case "FETCH_SUCCESS": 
        console.log(state)
        return Object.assign({}, state, {
            rate: action.rate,
            max: action.max,
            min: action.min,
            time: action.time,
            timeCounter: action.timeCounter,
            default: action.default,
            actRate: action.actRate,
            actTime: action.actTime,
            monthRate: action.monthRate,
            monthCommission: action.monthCommission,
            totalCommission: action.totalCommission
          });
          case "CHANGE_TIME": 
          return Object.assign({}, state, {
            actTime: action.actTime
          });
          case "CHANGE_RATE": 
          return Object.assign({}, state, {
            actRate: action.actRate
          });
          case "SET_COMMISSION": 
          console.log("ciao" + action.monthRate)
          return Object.assign({}, state, {
            monthRate: action.monthRate,
            monthCommission: action.monthCommission,
            totalCommission: action.totalCommission
          });
        default:
            return state;
    }
}
export default rootReducer;